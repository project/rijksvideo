CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements


INTRODUCTION
------------

The 'Rijksvideo' module provides a Rijksvideo media entity type.
This allows a site builder to add Rijksvideos through the Media module -
instead of using nodes which were used in the previous version of this module.


REQUIREMENTS
------------

In order to use this module you need to install the Media module.
