<?php

namespace Drupal\Tests\rijksvideo\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests for the Rijksvideo module.
 *
 * @group rijksvideo
 */
class RijksvideoPlayerTest extends KernelTestBase {

  /**
   * Modules that need to be enabled for this test.
   *
   * @var static
   */
  protected static $modules = [
    'file',
    'media',
    'rijksvideo',
  ];

  /**
   * Test normalzing URLs.
   */
  public function testNormalizeUrl() {
    $urls = [
      'http://rijksoverheid.nl' => 'https://rijksoverheid.nl',
      'https://rijksoverheid.nl' => 'https://rijksoverheid.nl',
    ];

    foreach ($urls as $input => $normalized) {
      $this->assertEquals($normalized, rijksvideo__normalize_url($input));
    }
  }

}
