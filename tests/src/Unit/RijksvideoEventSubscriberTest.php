<?php

namespace Drupal\Tests\rijksvideo\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\rijksvideo\EventSubscriber\RijksvideoEventSubscriber;

/**
 * Tests for the RijksvideoEventSubscriber.
 *
 * @group rijksvideo
 */
class RijksvideoEventSubscriberTest extends UnitTestCase {

  /**
   * Test if the getSubscribedEvents function is not empty.
   */
  public function testGetSubscribedEventsNotEmpty() {
    $this->assertNotEmpty(RijksvideoEventSubscriber::getSubscribedEvents());
  }

}
