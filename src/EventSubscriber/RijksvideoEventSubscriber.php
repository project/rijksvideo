<?php

namespace Drupal\rijksvideo\EventSubscriber;

use Drupal\Component\Utility\Bytes;
use Drupal\Component\Utility\Html;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\rijksvideo\Event\RijksvideoEvent;
use GuzzleHttp\ClientInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Enrich the creation of a new media entity.
 */
class RijksvideoEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new RijksvideoEventSubscriber object.
   *
   * @param \Drupal\file\FileRepositoryInterface $fileRepository
   *   The file repository service.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   */
  public function __construct(
    protected FileRepositoryInterface $fileRepository,
    protected ClientInterface $httpClient,
    protected LoggerChannelFactoryInterface $loggerFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RijksvideoEvent::RIJKSVIDEO_MEDIA_PRESAVE][] = ['fillMetaData'];
    return $events;
  }

  /**
   * Enrich the creation of a new media entity.
   *
   * @param \Drupal\rijksvideo\Event\RijksvideoEvent $event
   *   The Rijksvideo event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function fillMetaData(RijksvideoEvent $event) {
    /** @var \Drupal\media\MediaInterface $entity */
    $entity = $event->getEntity();

    if ($entity->bundle() !== "rijksvideo") {
      return;
    }

    /** @var \Drupal\file\FileInterface $file */
    $file = $entity->get('field_rijksvideo_file')->entity;
    if (!$file) {
      return;
    }

    $xmlData = file_get_contents($file->getFileUri());
    $result = new \SimpleXMLElement($xmlData);

    $rijksvideo = [];
    $title = (string) $result->title;
    $entity->setName($title);

    foreach ($result->files->children() as $file) {
      $url = (string) $file;
      // Get attributes from file.
      $attributes = (array) $file->attributes();
      $attributes = $attributes['@attributes'];

      switch ($attributes['type']) {
        case 'still_m':
          $ext = pathinfo($url, PATHINFO_EXTENSION);
          $thumb = $this->rijksvideoFetchFile($url, $title, '_thumb.' . $ext);
          $entity->set('field_rijksvideo_thumbnail', $thumb);
          $entity->set('thumbnail', $thumb);
          break;

        case 'subtitle_srt':
          $entity->set('field_rijksvideo_caption', $this->rijksvideoFetchFile($url, $title, '_subtitle.srt'));
          break;

        case 'text':
          $entity->set('field_rijksvideo_transcription', $this->rijksvideoFetchTranscription($url));
          $entity->set('field_rijksvideo_trans_url', rijksvideo__normalize_url($url));
          break;

        case 'audio_mp3':
          $entity->set('field_rijksvideo_audio', rijksvideo__normalize_url($url));
          break;

        // Video files.
        case 'web_webm':
        case 'web_mp4':
        case 'web_wmv':
        case 'web_flv':
        case 'web_hd':
        case 'web_ogv':
          $rijksvideo[$attributes['type']]['url'] = rijksvideo__normalize_url($url);
          $rijksvideo[$attributes['type']]['size'] = $this->formatBytes(Bytes::toNumber($attributes['size']));
          $rijksvideo[$attributes['type']]['type'] = strtoupper(str_replace(['web_', 'hd'], ['', 'mp4 hd'], $attributes['type']));
          break;
      }
    }

    $entity->set('field_rijksvideo_duration', $result->duration);
    $entity->set('field_rijksvideo_sources', json_encode($rijksvideo));

    $event->setEntity($entity);
  }

  /**
   * Fetch file or subittles.
   *
   * @param string $url
   *   The URL that needs to be fetched.
   * @param string $title
   *   The title of the file.
   * @param string $type
   *   The type of file.
   *
   * @return mixed
   *   Return either the file or subtitles.
   */
  protected function rijksvideoFetchFile($url, $title, $type) {
    if (empty($url) || !$this->urlExists($url)) {
      return FALSE;
    }

    $url = rijksvideo__normalize_url($url);
    $title = $this->sanitizeFilename($title);
    $destination = 'public://rv_data_' . $title . $type;

    if ($type !== '_subtitle.srt') {
      $data = (string) $this->httpClient->request('GET', $url)->getBody();
      return $this->fileRepository->writeData($data, $destination);
    }

    // File is a subtitle, download its content.
    $response = $this->httpClient->request('GET', $url, ['headers' => ['Accept' => 'text/plain']]);
    $file_content = (string) $response->getBody();

    // Convert SRT files to VTT.
    $file = $this->convertSrtToVtt($file_content);

    // Save the new subtitle file.
    $subtitles = $this->fileRepository->writeData($file, str_replace('.srt', '.vtt', $destination));

    return $subtitles;
  }

  /**
   * Check whether a URL exists.
   *
   * @param string $url
   *   The URL that needs to be checked.
   *
   * @return bool
   *   Returns if the URL exists
   */
  protected function urlExists(string $url): bool {
    $url = rijksvideo__normalize_url($url);

    try {
      $response = $this->httpClient->request('HEAD', $url);

      if ($response->getStatusCode() == 200) {
        return TRUE;
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('rijksvideo')->error("Could not download file: " . $url . " - " . $e->getMessage());
    }

    $this->loggerFactory->get('rijksvideo')->error("Could not download file: " . $url . " - It probably doesn't exist!");

    return FALSE;
  }

  /**
   * Sanitize filename.
   *
   * @param string $filename
   *   The filename that needs to be sanitized.
   *
   * @return string
   *   The sanitized filename.
   */
  protected function sanitizeFilename(string $filename): string {
    $filename = Html::decodeEntities($filename);
    $filename = preg_replace('/[^a-zA-Z0-9_]+/', '_', $filename);
    $filename = strtolower($filename);

    return trim($filename, '_');
  }

  /**
   * Converts file's content (.srt) to vtt format.
   *
   * @param string $file_content
   *   Content of file that will be converted.
   *
   * @return string
   *   Internal format
   */
  protected function convertSrtToVtt(string $file_content): string {
    if (strlen($file_content) < 10) {
      return $file_content;
    }

    // Array - where file content will be stored.
    $internal_format = [];
    $blocks = preg_split("#\n\s*\n#Uis", trim($file_content));

    foreach ($blocks as $block) {
      // Separate all block lines.
      $lines = preg_split('/\r\n|\r|\n/', trim($block));
      // One the second line there is start and end times.
      $times = explode(' --> ', $lines[1]);
      $internal_format[] = [
        'start' => static::srtTimeToInternal($times[0]),
        'end' => static::srtTimeToInternal($times[1]),
      // Get all the remaining lines from block (if multiple lines of text)
        'lines' => array_slice($lines, 2),
      ];
    }

    $file_content = "WEBVTT\r\n\r\n";

    foreach ($internal_format as $block) {
      $start = static::internalTimeToVtt($block['start']);
      $end = static::internalTimeToVtt($block['end']);
      $lines = implode("\n", $block['lines']);
      $file_content .= $start . ' --> ' . $end . "\r\n";
      $file_content .= $lines . "\r\n";
      $file_content .= "\r\n";
    }
    $file_content = trim($file_content);

    return $file_content;
  }

  /**
   * Convert .srt file format to internal time format (float in seconds)
   *
   * Example: 00:02:17,440 -> 137.44.
   *
   * @param string $srt_time
   *   The SRT time.
   *
   * @return float
   *   The internal time.
   */
  protected static function srtTimeToInternal(string $srt_time): float {
    $parts = explode(',', $srt_time);
    $only_seconds = strtotime("1970-01-01 {$parts[0]} UTC");
    $milliseconds = (float) ('0.' . $parts[1]);
    $time = $only_seconds + $milliseconds;

    return $time;
  }

  /**
   * Convert internal time to SRT time.
   *
   * @param string $internal_time
   *   The internal time.
   *
   * @return string
   *   The SRT time.
   */
  protected static function internalTimeToVtt(string $internal_time): string {
    // 1.23
    $parts = explode('.', $internal_time);
    // 1
    $whole = $parts[0];
    // 23
    $decimal = isset($parts[1]) ? substr($parts[1], 0, 3) : 0;
    $srt_time = gmdate("H:i:s", floor($whole)) . '.' . str_pad($decimal, 3, '0', STR_PAD_RIGHT);

    return $srt_time;
  }

  /**
   * Fetch transcription.
   *
   * @param string $url
   *   The URL that needs to be fetched.
   *
   * @return string
   *   Returns the transcription.
   */
  protected function rijksvideoFetchTranscription(string $url): string {
    $transcription = '';
    if (!empty($url) && $this->urlExists($url)) {
      $url = rijksvideo__normalize_url($url);

      // File is a subtitle, download its content.
      $transcription = file_get_contents($url);
      $transcription = iconv(mb_detect_encoding($transcription, mb_detect_order(), TRUE), "UTF-8//TRANSLIT//IGNORE", $transcription);
    }

    return $transcription;
  }

  /**
   * Format bytes to string.
   *
   * @param int $bytes
   *   The number of bytes.
   * @param int $precision
   *   The precision.
   *
   * @return string
   *   The formatted string of bytes.
   */
  protected function formatBytes(int $bytes, $precision = 2): string {
    $units = ['B', 'KB', 'MB', 'GB', 'TB'];

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    $bytes /= pow(1024, $pow);

    return round($bytes, $precision) . ' ' . $units[$pow];
  }

}
