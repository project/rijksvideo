<?php

namespace Drupal\rijksvideo\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Wraps a node insertion for event listeners.
 */
class RijksvideoEvent extends Event {
  const RIJKSVIDEO_MEDIA_PRESAVE = 'rijksvideo.media.presave';

  /**
   * Media entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Constructs a media insertion event object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Media entity.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Get the inserted entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Return the Media entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Set the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The media entity.
   */
  public function setEntity($entity) {
    $this->entity = $entity;
  }

}
