<?php

namespace Drupal\rijksvideo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;

/**
 * Settings form for the Rijksvideo Player v2 module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Constructs a SettingsForm object.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator service.
   */
  public function __construct(private CacheTagsInvalidatorInterface $cache_tags_invalidator) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rijksvideo.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rijksvideo_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['only_show_download_accordion'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only show a download accordion'),
      '#description' => $this->t('Check this box if you only want to show a download accordion instead of the default accordions.'),
      '#default_value' => $this->config('rijksvideo.settings')->get('only_show_download_accordion'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('rijksvideo.settings')
      ->set('only_show_download_accordion', $form_state->getValue('only_show_download_accordion'))
      ->save();
    // Empty cachetag for config.
    $this->cache_tags_invalidator->invalidateTags(['config:rijksvideo.settings']);
    parent::submitForm($form, $form_state);
  }

}
