/**
 * @file
 * Constructs all the rijksvideos.
 */

((Drupal, once, Video) => {
  /**
   * Attaches the rijksvideo behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior.
   */
  Drupal.behaviors.rijksvideo = {
    attach(context) {
      const $videos = once(
        'rijksvideo',
        '.rijksvideo',
        context.parentNode || context,
      );

      $videos.forEach(($video) => {
        const video = new Video($video, {
          $video: '.rijksvideo__video',
          plugins: {
            ad: {
              $handler: '.rijksvideo__ad',
              activeClass: 'is-active',
            },
            fullscreen: {
              $handler: '.rijksvideo__fullscreen',
              fullscreenClass: 'is-fullscreen',
              activeClass: 'is-active',
            },
            mute: {
              $handler: '.rijksvideo__mute',
              muteClass: 'is-muted',
            },

            play: {
              $handler: '.rijksvideo__play',
              showClass: 'is-shown',
            },
            playpause: {
              $handler: '.rijksvideo__playpause',
              playClass: 'is-playing',
            },
            progress: {
              $el: '.rijksvideo__progress',
              $bar: '.rijksvideo__progress-bar',
              $loaded: '.rijksvideo__progress-loaded',
            },
            caption: {
              $handlers: '.rijksvideo__caption',
              activeClass: 'is-active',
            },
            time: {
              $currentTime: '.rijksvideo__current-time',
              $duration: '.rijksvideo__duration',
            },
            volume: {
              $el: '.rijksvideo__volume',
              $bar: '.rijksvideo__volume-bar',
            },
          },
        });

        // Handle play.
        $video.addEventListener('play.video', () => {
          const $playpause = video.plugins.playpause.$handler;
          $playpause.setAttribute('aria-label', $playpause.dataset.pauseLabel);
        });

        // Handle pause.
        $video.addEventListener('pause.video', () => {
          const $playpause = video.plugins.playpause.$handler;
          $playpause.setAttribute('aria-label', $playpause.dataset.playLabel);
        });

        // Handle volume change.
        $video.addEventListener('volumechange.video', (e) => {
          const $mute = video.mute.$handler;

          $mute.setAttribute(
            'aria-label',
            $mute.dataset[`mute${e.detail.muted ? 'Off' : 'On'}Label`],
          );
        });

        // Handle caption.
        $video.addEventListener('caption.video', (e) => {
          e.detail.$handlers.forEach(($handler) => {
            $handler.setAttribute(
              'aria-label',
              $handler.dataset[
                `caption${$handler.track.mode === 'showing' ? 'Off' : 'On'}Label`
              ],
            );
          });
        });

        // Handle audio description.
        $video.addEventListener('ad.video', (e) => {
          const $ad = video.plugins.ad.$handler;

          $ad.setAttribute(
            'aria-label',
            $ad.dataset[`ad${e.detail.state === 'on' ? 'Off' : 'On'}Label`],
          );
        });

        // Handle fullscreen.
        $video.addEventListener('fullscreen.video', (e) => {
          const $fullscreen = video.plugins.fullscreen.$handler;

          $fullscreen.setAttribute(
            'aria-label',
            $fullscreen.dataset[
              `fullscreen${e.detail.fullscreen ? 'Exit' : 'Enter'}Label`
            ],
          );
        });
      });
    },
  };
})(Drupal, once, Video);
