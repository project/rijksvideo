# Changelog

## 1.0.x - 2025-02-26
### Changes
- Enhanced Styling – Improved visual consistency and design.
- Refactored CSS, JavaScript, and Templates – Increased maintainability and efficiency.
- Fixed Subtitle Format Conversion – Resolved issues with SRT to VTT conversion.
- General Code Improvements – Optimized and cleaned up code for better performance.

## 1.0.x - 2025-01-21
### Changes
- Enhanced styling update
- Added Audio description button
- A new field has been added to save the transcribed text link. All existing Rijksvideo media content will automatically be updated and populate this field through an update hook.
- An accordion Layout  settings form has been added, allowing users to choose the accordion layout for better customization.

## 1.0.x - 2024-12-11
### Changes
- Config files optional so DICTU projects can migrate to the drupal.org version
- Template variable videosource to video_source
- Add and edit form updated with info about auto-fill based on XML upload
- Use of dependency injection
- Sanitize filenames
- Move theme library to template instead of preprocess
- Removed duplicate function rijksvideo__normalize_url() also in subscriber

### Fixed
- Drupal 11 proof. Used drupal-check
- Updated deprecated function system_retrieve_file()
