<?php

/**
 * @file
 * Rijksvideo install file.
 */

use Drupal\field\Entity\FieldConfig;
use Drupal\file\Entity\File;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Implements hook_uninstall().
 *
 * Get the settings configuration and delete it.
 *
 * @throws \Exception
 */
function rijksvideo_uninstall() {
  $config = \Drupal::configFactory()->getEditable('rijksvideo.settings');
  $config->delete();
}

/**
 * Implements hook_update_N().
 */
function rijksvideo_update_10001() {
  $field_name = 'field_rijksvideo_trans_url';

  $field_storage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->load("media.$field_name");
  $field = \Drupal::entityTypeManager()->getStorage('field_config')->load("media.rijksvideo.$field_name");

  if (!$field_storage && !$field) {

    // Ensure the UUIDs are fixed to prevent conflicts during config import.
    $uuid_field_storage = 'e70609e1-972a-427c-acdf-789c8b9672bf';
    $uuid_field = 'b678e709-2ff8-4cf4-900c-a8d2045976f3';

    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'media',
      'type' => 'string',
      'uuid' => $uuid_field_storage,
      'settings' => [
        'max_length' => 255,
      ],
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'rijksvideo',
      'label' => 'Transcription URL',
      'description' => 'Auto-filled based on the Rijksvideo XML file',
      'uuid' => $uuid_field,
    ]);
    $field->save();

    $form_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('media.rijksvideo.default');

    $form_display->setComponent($field_name, [
      'type' => 'string_textfield',
      'weight' => 10,
    ]);

    $form_display->save();

    $media_storage = \Drupal::entityTypeManager()->getStorage('media');
    $query = $media_storage->getQuery()
      ->condition('bundle', 'rijksvideo')
      ->accessCheck(FALSE);
    $result = $query->execute();

    foreach ($result as $mid) {
      $entity = $media_storage->load($mid);

      $fid = $entity->get('field_rijksvideo_file')->target_id;
      $file = File::load($fid);

      $xmlData = file_get_contents($file->getFileUri());
      $result = new \SimpleXMLElement($xmlData);

      foreach ($result->files->children() as $file) {
        $url = (string) $file;
        $attributes = (array) $file->attributes();
        $attributes = $attributes['@attributes'];

        if ($attributes['type'] == 'text') {
          $url = rijksvideo__normalize_url($url);
          $entity->set('field_rijksvideo_trans_url', $url);
          $entity->save();
        }
      }
    }
  }
}
